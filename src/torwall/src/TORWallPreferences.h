#ifndef __TORWALLPREFERENCES_H__
#define __TORWALLPREFERENCES_H__

#include <panel-applet.h>
#include <glade/glade.h>

typedef struct TORWallPreferences {
  PanelApplet *applet;
  GladeXML *xml;
} TORWallPreferences;

void torwall_preferences_display(TORWallPreferences *preferences);
TORWallPreferences* torwall_preferences_new(PanelApplet *applet);

#endif
